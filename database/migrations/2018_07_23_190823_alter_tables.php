<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comments', function (Blueprint $table) {

            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('advert_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('advert_id')->references('id')->on('adverts');
        });
        Schema::table('adverts', function (Blueprint $table) {
            $table->UnsignedBigInteger('user_id');
            $table->UnsignedBigInteger('category_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('category_id')->references('id')->on('subcategories');
        });
        Schema::table('favorites', function (Blueprint $table) {
            $table->UnsignedBigInteger('user_id');
            $table->UnsignedBigInteger('advert_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('advert_id')->references('id')->on('adverts');
        });
        Schema::table('subcategories', function (Blueprint $table) {
            $table->UnsignedBigInteger('main_category_id');
            $table->foreign('main_category_id')->references('id')->on('maincategories');
        });
        Schema::table('metrics', function (Blueprint $table) {
            $table->UnsignedBigInteger('advert_id');
            $table->foreign('advert_id')->references('id')->on('adverts');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comments', function (Blueprint $table) {

            $table->dropForeign(['user_id']);
            $table->dropForeign(['advert_id']);
        });
        Schema::table('adverts', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['category_id']);

        });
        Schema::table('favorites', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['advert_id']);

        });

        Schema::table('subcategories', function (Blueprint $table) {
            $table->dropForeign(['main_category_id']);
        });

        Schema::table('metrics', function (Blueprint $table) {

            $table->dropForeign(['advert_id']);
        });

    }
}
