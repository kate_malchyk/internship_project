<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/',  function () {
    return view('welcome');
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/register', ['uses' => 'RegisterController@register', 'as' => 'register'
    ])
;
Route::post('/signin', ['uses' => 'LogInController@signIn', 'as' => 'signin'
    ]);
Route::get('/logout', ['uses' => 'UserController@getLogout', 'as' => 'logout'

]);


    Route::get('/dashboard', [
        'uses' => 'AdvertController@getDashboard',
        'as' => 'dashboard'
        //'middleware' => 'auth:api'
    ]);
    Route::post('/create-advert', [
        'uses' => 'AdvertController@postCreateAdvert',
        'as' => 'post.create'
    ]);

    Route::get('/delete-advert/{advert_id}', [
        'uses' => 'AdvertController@getDeleteAdvert',
        'as' => 'advert.delete'

    ]);

    Route::get('/account', [
        'uses' => 'UserController@getAccount',
        'as' => 'account'
    ]);

Route::post('/updateaccount', [
    'uses' => 'UserController@postSaveAccount',
    'as' => 'save.account'
]);





//Route::patch('/user/{user}', 'UserController@update')->middleware('auth:api');

//Route::get('user/{id}','UserController@show');