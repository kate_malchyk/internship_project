<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainCategory extends Model
{
    protected $fillable = ['name'];
    public function subcategories(){
        return $this->hasMany(SubCategory::class,'main_category_id');
    }
}
