<?php
/**
 * Created by PhpStorm.
 * User: kate_pc
 * Date: 25.07.18
 * Time: 13:45
 */

namespace App\Transformers;


use League\Fractal\TransformerAbstract;
use App\User;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user){

        return [
            'firstName' => $user->firstName,
            'avatar' => $user->avatar(),
        ];
    }

}