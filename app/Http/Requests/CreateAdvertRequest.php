<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateAdvertRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return false;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255|min:5',
            'description' =>'required|string|min:10',
            //'image' => 'string',
            'price' =>'required|numeric',
            //'user_id' =>'required|numeric|exists:users,id',
            //'category_id' =>'required|numeric|exists:sub_categories,id'
        ];
    }
}

