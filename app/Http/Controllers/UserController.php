<?php

namespace App\Http\Controllers;
use App\Http\Requests\UpdatePassworsRequest;
use App\User;
use Hash;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateUserRequest;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Bridge\UserRepository;
use Spatie\Fractal\Fractal;
use Illuminate\Support\Facades\Redis;


class UserController extends Controller

{
    protected $users;

public function getLogout(){

    Auth::logout();
    return redirect()->route('home');
}
 public function getAccount(){
    return view('account',['user' => Auth::user()]);
 }
 public function postSaveAccount(Request $request){

    $this->validate($request, [
        'firstName' => 'required|max:255',
        'lastName' => 'required|string|max:255',
        'email' => 'required|email|string||unique:users,email',
        'city' => 'string|min:3|max:255',

    ]);
    $user =Auth::user();
    $user->firstName = $request['firstName'];
    $user->update();
    //$file =$request->file('image');
    // $filename =$request['first_name'].'-'.$user->id. 'jpg';
 }



    //обновление пароля пользователя

  /* public function updatePassword(UpdatePassworsRequest $request, $id){

        $user = User::findOrFail($id);
        $user->password = bcrypt($request->password);

        $user->fill([
            'password' => Hash::make($request->newPassword)
        ])->save();
    }


    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    public function update(UpdateUserRequest $request, User $user){
        /**
         * @param UpdateUserRequest $request
         * @param User $user
         * @return Fractal


        $user = User::findOrFail($user);
        $this->authorize($user);
        $user->firstName = $request->get('firstName', $user->firstName);
        $user->lastName = $request->get('lastName', $user->lastName);
        $user->city = $request->get('city', $user->city);
        $user->save();

        return fractal()
            ->item($user)
            ->transformWith(new UserTransformer)
            ->toArray();

    }
    // srt middleware

/*
    //show user's profile

    public function showProfile($id){

   $user = Redis::get('user:profile:'.$id);
        return view('userProfile.index',['user'=> $user]);
    }

    //show all register userspublic function index(){
        $users = User::all();
        return view('userProfile.index',['users'=>$users]);
    }
   */


   /* public function update(User $user){

        $this->validate(request(), [
            'firstName' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'email' => 'required|email|string||unique:users,email',
            'password' => 'required|string|min:3',
            'passwordToken' => 'required|string|min:3',
            'city' => 'string|min:3|max:255'
        ]);

        $user->firstName = request('firstName');
        $user->lastName = request('lastName');
        $user->email = request('email');
        $user->password = bcrypt(request('password'));
        $user->passwordToken = request('passswordToken');
        $user->city = request('city');

        $user->save();

    }*/

}
