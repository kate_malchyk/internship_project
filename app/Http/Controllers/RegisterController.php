<?php

namespace App\Http\Controllers;

use App\Transformers\UserTransformer;
use Illuminate\Http\Request;
use App\Http\Requests\SignUpRequest;
use App\User;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*public function signUpAction(SignUpRequest $request){
        $password = Hash::make($request -> get('password'));
    }
    */
    public function register(SignUpRequest $request)
    {

        //profileData
        $user = new User;

        $user->firstName = $request->firstName;
        $user->lastName = $request->lastName;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->passwordToken = $request->passwordToken;
        $user->city = $request->city;

        $user->save();

        Auth::login($user);


       /* return response()
            ->json([
            'status' => 'success',
            'code' => 200
            ]);*/
        return redirect()->back();
            /*[
             fractal()
            ->item($user)
            ->transformWith(new UserTransformer)
            ->toArray(),
             response()
                ->json([
                    'status' => 'success',
                    'code' => 200,
            ]),
            redirect()->route('dashboard'),
        ]; */

    }

}
