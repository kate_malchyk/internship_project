<?php

namespace App\Http\Controllers;

use App\Http\Requests\SignInRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LogInController extends Controller
{
    //
    public function signIn(Request $request){
        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])){
            return redirect()->route('dashboard');
        }
        return redirect()->back();

    }
}




/* public function signIn(SignInRequest $request)
 {
     $email = $request->get('email');
     $password = $request->get('password');

     $user = User::where('email', $email)->first();

     if ($user == null) {
         return response()->json([
             'status' => false,
             'code' => 403
         ]);

     }
     $passwordFromDb = $user->password;

     if (Hash::check($password, $passwordFromDb)) {

         $session = app('session');
         $token = $session->makeSession($user);
         Cache::add($token, json_encode($user));

         return response()->json([
             'status' => 'success',
             'code' => 200,
             'token => $token'
         ]);

     }
     return response()->json([
         'status' => false,
         'code' => 403,
     ]);
 }

 public function checkSession(Request $request)
 {
     $authToken = $request->header('X-Access-TOKEN');
 }
 private function validate(array $data)
 {

 }*/
