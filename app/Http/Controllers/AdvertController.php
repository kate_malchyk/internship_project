<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAdvertRequest;
use Illuminate\Http\Request;
use App\Advert;
use App\User;
use Illuminate\Support\Facades\Auth;

class AdvertController extends Controller
{
    public function getDashboard(){

        $adverts = Advert::orderBy('created_at', 'desc')->get();

        return view('dashboard', ['adverts' =>$adverts]);

    }
    //Validation

    public function postCreateAdvert(CreateAdvertRequest $request){

        /*'title',
        'description',
        'image',
        'price',
        $createAdvertData = $request->post();
        $createAdvertData['user_id'] =1;

        'status',
         */
        $advert = new Advert();
        $advert->title = $request['title'];
        $advert->description = $request['description'];
        $advert->price = $request['price'];
        $advert->save();
        //$request->user()->adverts()->save($advert);

        return redirect()->route('dashboard');
    }
     public function getDeleteAdvert($advert_id){
        $advert =Advert::where('id', $advert_id)->first();
       /* if(Auth::user()!= $advert->user){
            return redirect()->back();
        }*/
        $advert->delete();
        return redirect()->route('dashboard')->with(['message' =>'Successfully deleted!']);
     }


}
