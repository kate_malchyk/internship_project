<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metric extends Model
{
    protected $fillable = [
        'referer',
        'user_agent'
    ];
    public function advert()
    {
        return $this->belongsTo(Advert::class);
    }
}
