<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advert extends Model
{
    protected $fillable = [
        'title',
        'description',
        'image',
        'price',
        'status',
        'city'
    ];
    public function comments(){
        return $this->hasMany(Comment::class);
    }
    public function favorites(){
        return $this->hasMany(Favorite::class);
    }
    public function metrics(){
        return $this->hasMany(Metric::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function subcategory()
    {
        return $this->belongsTo(SubCategory::class.'category_id');
    }
}
