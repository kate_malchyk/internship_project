<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\Authenticatable;

class User extends Model implements Authenticatable
{
    use Notifiable, HasApiTokens;
    use \Illuminate\Auth\Authenticatable;
    /*
     * @var array
     */


    public $fillable = [
        'firstName', 'lastName', 'email', 'password', 'passwordToken', 'city',
    ];

     /*public $firstName;
     public $lastName;
     public $email;
     public $password;
     public $passwordToken;
     public $city;*/

    protected $hidden =[
        'password',
        'passwordToken'
    ];

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function favorites(){
        return $this->hasMany(Favorite::class);
    }
    public function adverts(){
        return $this->hasMany(Advert::class);
    }


    public function avatar()
{
    return 'https://www.gravatar.com/avatar/' . md5($this->email) . '?s=45&d=mm';
}

/*public function check(User $user){

        return $this->id === $user->id;
}
*/
}

