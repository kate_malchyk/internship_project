<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $fillable = [
        'name'
    ];
    public function adverts(){
        return $this->hasMany(Advert::class,'category_id');
    }
    public function maincategory()
    {
        return $this->belongsTo(MainCategory::class, 'main_category_id');
    }
}
