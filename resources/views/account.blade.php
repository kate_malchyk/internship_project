@extends('layouts.master')

@section('title')
    User Profile
    @endsection
    @extends('layouts.master')

@section('title')
    Account
@endsection

@section('content')
    <section class="row new-post">
        <div class="col-md-6 col-md-offset-3">

            <header><h3>Your Account</h3></header>
            <form action="{{ route('account') }}" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="first-name">First Name(user name)</label>
                </div>
                <div class="form-group">
                    <label for="last-name">Last Name(user name)</label>
                </div>
                <div class="form-group">
                    <label for="email"> Email(user email)</label>
                </div>
                <div class="form-group">
                    <label for="city">City (user city)</label>
                </div>
                <div class="form-group">
                    <label for="image">Image (only .jpg)</label>
                    <input type="file" name="image" class="form-control" id="image">
                </div>
                <button type="submit" class="btn btn-primary">Save Account</button>
                <input type="hidden" value="{{ Session::token() }}" name="_token">
            </form>
        </div>
    </section>

@endsection

