@extends('layouts.master')

@section('title')
    Home
@endsection
 @section('content')
<h1>Bulletin Board</h1>
     <section class = "row new-post">
         <div class ="col-md-6 col-md-offset-10">

             <header>
                 <h3>There you can advertise whatever you want!</h3>
             </header>
             <form action="{{route('post.create')}}" method="post">

                 <div class = "form-group ">
                     <label for = "title">Advert title:</label>
                     <input class="form-control" type="text" name="title" id ="title">
                 </div>

                 <div class = "form-group">
                     <textarea class ="form-control" name="description" id="new-advert" rows = "5" placeholder="Your Advert"></textarea>
                 </div>

                 <div class = "form-group ">
                     <label for = "price">Set a price:</label>
                     <input class="form-control" type="number" name="price" id ="price">
                 </div>

                 <button type ="submit" class ="btn btn-primary">Create an advert </button>
                 <input type="hidden" value="{{Session::token()}}" name="_token">
             </form>
         </div>
     </section>

     <section class="row posts">
         <div class ="col-md-6  col-md-3-offset">
             <header>
                 <h3>Other adverts...</h3>
             </header>
                 @foreach($adverts as $advert)
             <article class = "post">
                 <p>Title: {{ $advert ->title}}</p>
                     <p>{{$advert->description}}</p>
                    <p>Price: {{$advert->price}}</p>
                 <div class="info">
                     Posted by Kate on {{$advert->created_at}}
                 </div>
                 <div class ="interaction">
                     <a href="#">Comment</a> |
                     <a href = "#" >Edit</a> |
                     <a href="#">Favorite</a> |
                     <a href = "{{ route('advert.delete',['advert_id' => $advert->id]) }}">Delete</a>
                 </div>
             </article>
             @endforeach
         </div>
     </section>

<div class="modal fade" tabindex="-1" role="dialog" id ="edit-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit advert</h4>
            </div>
            <div class="modal-body">
                <p>One fine body&hellip;</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

