@extends('layouts.master')

@section('title')
    Welcome page
    @endsection

@section('content')

    <h1 style="font-size: 260%; text-align: center; color: #cd66cc">Welcome to Bulletin Board</h1>
    <div class ="row">
    <div class ="col-md-6">
        <h3 style ="color:#1E90FF">Register</h3>
    <form action="{{ route('register') }}" method ="post">
    <div class = "form-group ">
        <label for = "firstName">Your first name</label>
    <input class="form-control" type="text" name="firstName" id ="firstName">
    </div>

        <div class = "form-group">
        <label for = "lastName">Your last name</label>
        <input class="form-control" type="text" name="lastName" id ="lastName">
    </div>

        <div class = "form-group">
            <label for = "email">Your email</label>
            <input class="form-control" type="text" name="email" id ="email">
        </div>

        <div class = "form-group">
            <label for = "password">Your password</label>
            <input class="form-control" type="password" name="password" id ="password">
        </div>
        <div class = "form-group">
            <label for = "passwordToken">Your password token</label>
            <input class="form-control" type="text" name="passwordToken" id ="passwordToken">
        </div>
        <div class = "form-group">
            <label for = "city">Your city</label>
            <input class="form-control" type="text" name="city" id ="city">
        </div>
        <button type="submit" class="btn btn-primary">Sign up</button>
        <input type="hidden" name="_token" value="{{Session::token() }}">
    </form>
    </div>

        <div class ="col-md-6">
            <h3 style ="color:#1E90FF">Log In</h3>
            <form action = "{{route('signin')}}" method ="post">

                <div class = "form-group">
                    <label for = "email">Your email</label>
                    <input class="form-control" type="text" name="email" id ="email">
                </div>

                <div class = "form-group">
                    <label for = "password">Your password</label>
                    <input class="form-control" type="password" name="password" id ="password">
                </div>

                <button type="submit" class="btn btn-primary">Log in</button>
                <input type="hidden" name="_token" value="{{Session::token() }}">
            </form>
        </div>
    </div>
@endsection